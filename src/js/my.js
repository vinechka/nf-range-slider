var nfRangeSlider = document.getElementsByClassName('nf-range__slider');

for (var i = 0; i <= nfRangeSlider.length; i++) {
    var start, min, max, caption, step;

    caption = nfRangeSlider[i].dataset.caption;
    start = parseInt(nfRangeSlider[i].dataset.start);
    min = parseInt(nfRangeSlider[i].dataset.min);
    max = parseInt(nfRangeSlider[i].dataset.max);
    step = parseInt(nfRangeSlider[i].dataset.step);

    noUiSlider.create(nfRangeSlider[i], {
        start: [start > 0 ? start : 1],
        step: step > 0 ? step : 1,
        connect: [true, false],
        range: {
            'min': min > 0 ? min : 1,
            'max': max > 0 ? max : 1
        }
    });

    nfRangeSlider[i].noUiSlider.on('update', function (values, handle) {
        var newCaption = this.target.dataset.caption,
            value = parseInt(values[handle]).toLocaleString('ru');
        this.target.previousElementSibling.innerHTML = value + ' ' + newCaption;
    })
}