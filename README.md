## Range slider
Добавление бегунка:

**Подключение стилей:**
```html
<link href="/nf-range-slider/libs/nouislider.min.css" rel="stylesheet" type="text/css">
<link href="/nf-range-slider/dist/nf-range-slider.css" rel="stylesheet" type="text/css">
```

**Подключение скриптов:**
```html
<script src="/nf-range-slider/libs/nouislider.min.js"></script>
<script src="/nf-range-slider/dist/nf-range-slider.js"></script>
```

**Разметка:**

```html
<!--
    data-caption - подпись к результату
    data-start - точка начала отчета
    data-min - минимальное значение
    data-max - максимальное значение
    data-step - шаг
-->

<div class="nf-range">
    <div class="nf-range__value"></div>
    <div class="nf-range__slider" data-caption="мес" data-start="12" data-min="3" data-max="48" data-step="1"></div>
    <div class="nf-range__settings">
        <p>3</p>
        <p>48</p>
    </div>
</div>
```

- ```.nf-range``` - обертка
- ```.nf-range__value``` - вывод результата
- ```.nf-range__slider``` - обертка слайдера с настройками
- ```.nf-range``` - нажние подписи, заполняются руками :)

**Настройки:**

Настройки прописываются атрибутами к обертке слайдера ```.nf-range__slider```:

- **data-caption** - подпись к результату
- **data-start** - точка начала отчета
- **data-min** - минимальное значение
- **data-max** - максимальное значение
- **data-step** - шаг